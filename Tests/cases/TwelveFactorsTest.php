<?php
include_once(__DIR__ .  '/../conf/conf.php');
class TwelveFactorsTest extends PHPUnit_Framework_TestCase
{   

    public function testGetFactorById()
    {   
		$twelveFactors = new TwelveFactors();
		    
			$expected = array("factor_name" => "Codebase", "factor_description" => "One codebase tracked in revision control, many deploys"); 
			$this->assertSame($expected, $twelveFactors->getFactorById(1));
			
			$expected = array("factor_name" => "Dependencies", "factor_description" => "Explicitly declare and isolate dependencies");
			$this->assertSame($expected, $twelveFactors->getFactorById(2));
			
			$expected = array("factor_name" => "Config", "factor_description" => "Store config in the environment");
			$this->assertSame($expected, $twelveFactors->getFactorById(3));
			
			$expected = array("factor_name" => "Backing services", "factor_description" => "Treat backing services as attached resources");
			$this->assertSame($expected, $twelveFactors->getFactorById(4));
			
			$expected = array("factor_name" => "Build, release, run", "factor_description" => "Strictly separate build and run stages");
			$this->assertSame($expected, $twelveFactors->getFactorById(5));
			
		    $expected = array("factor_name" => "Processes", "factor_description" => "Execute the app as one or more stateless processes");
			$this->assertSame($expected, $twelveFactors->getFactorById(6));
			
			$expected = array("factor_name" => "Port binding", "factor_description" =>"Export services via port binding");
			$this->assertSame($expected, $twelveFactors->getFactorById(7));
			
			$expected = array("factor_name" => "Concurrency", "factor_description" =>"Scale out via the process model"  );
			$this->assertSame($expected, $twelveFactors->getFactorById(8));
			
			$expected = array("factor_name" => "Disposability", "factor_description" => "Maximize robustness with fast startup and graceful shutdown");
			$this->assertSame($expected, $twelveFactors->getFactorById(9));
			
			$expected = array("factor_name" => "Dev/prod parity", "factor_description" =>"Keep development, staging, and production as similar as possible");
			$this->assertSame($expected, $twelveFactors->getFactorById(10));
			
			$expected = array("factor_name" => "Logs", "factor_description" => "Treat logs as event streams");
			$this->assertSame($expected, $twelveFactors->getFactorById(11));
			
			$expected = array("factor_name" => "Admin processes", "factor_description" => "Run admin/management tasks as one-off processes");
			$this->assertSame($expected, $twelveFactors->getFactorById(12));
    }

    public function testGetFactorList()
    {   
	    $expected = array(array("id" => 1, "name" => "I. Codebase"), array("id" => 2, "name" => "II. Dependencies"), array("id" => 3, "name" => "III. Config"), array("id" => 4, "name" => "IV. Backing services"),
                       array("id" => 5, "name" => "V. Build, release, run"), array("id" => 6, "name" => "VI. Processes"), array("id" => 7, "name" => "VII. Port binding"), array("id" => 8, "name" => "VIII. Concurrency"),
					   array("id" => 9, "name" => "IX. Disposability"), array("id" => 10, "name" => "X. Dev/prod parity"), array("id" => 11, "name" => "XI. Logs"), array("id" => 12, "name" => "XII. Admin processes")); 
        
        $twelveFactors = new TwelveFactors();
        $this->assertSame($expected, $twelveFactors->getFactorList());
    }
}

